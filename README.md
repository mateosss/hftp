# Redes Laboratorio 2

*Mateo de Mayo, Catriel Fernandez, Naira Garibay.*  
*Correr con python >= 3.6*

## Estructuracion del Servidor

Hay 2 clases principales `Server`, `Connection`:
- `Server`: Se encarga de escuchar nuevas conexiones, al detectar un nuevo
  intento de conexión, crea un `socket` y una `Connection` con ese `socket`,
  luego llama a su método `handle`. Hasta que `handle` no termina, en principio,
  `Server` no puede atender otra conexión.
- `Connection`: Se encarga de gestionar la conexión mediante un loop que se
  queda esperando comandos del cliente, y al recibirlos, los procesa y devuelve
  una respuesta o un mensaje de error.

Al agregar threads al server, la estructura es muy similar, la única diferencia
es que, a la hora de crear una conexión, el `handle` no lo hacemos en el main
thread, si no que lo lanzamos en un thread nuevo.

## Decisiones de Diseño

### Procesamiento del Buffer

El loop de `Connection.handle` al agregar nuevos datos al buffer, llama a
`process_buffer`, el cual consume de `self.buffer` los comandos que están
completos pasándoselos a `parse`.

El método `parse` espera un comando completo sin `EOL` y aplica algunos
chequeos, `raise`-ando si no los cumple (estas excepciones son manejadas por
`@handle_errors` que está explicado en el título de abajo). Si todo va bien,
llama a la función correspondiente del comando la cual devuelve la respuesta o
hace un `raise` (que `@handle_errors` lo transforma en una respuesta de error).

En todo caso `process_buffer` siempre recibe de cada `parse` una respuesta para
el cliente, estas respuestas son las que luego devuelve a `Connection.handle`
para enviarlas al cliente.

### Manejo de Errores

Notamos que habría haber muchos casos a cubrir a la hora de responder un
mensaje: este podría estar tener mal sus argumentos, no ser un comando válido,
dar algún error propio del comando, o podría haber algún otro tipo error que no
esperábamos, etc. Para manejar esta complejidad decidimos que lo mejor sería
tener algún tipo de *wrapper* que manejara las excepciones de forma general, y
que cada comando manejara sus excepciones particulares en su función
correspondiente.

A este wrapper lo hicimos mediante un decorator y lo llamamos `@handle_errors`,
es una función que toma otra, `parse` en este caso, y la *"envuelve"*. En
particular, la wrappeamos con un `try` con dos `except`s con un pseudocódigo que
iría algo así:

```python
# Notar que `parse` devuelve la respuesta al comando
try: # Intentar parsear el comando
    res = encabezado_hftp_de(TODO_OK) + parse(comando)
except HFTPError as e: # Hubo algún error específico de HFTP?
    res = encabezado_hftp_de(e)
except Exception as e: # Hubo algún error de otro tipo?
    res = encabezado_hftp_de(InternalError)
return res
```

Donde `HFTPError` es una clase que hereda de `Exception` muy simple, lo único
que hace es poder `str`-inguizarse devolviendo la respuesta que hay que dar
al error según el protocolo, y tener una variable `close` que si es cierta,
índica que es un tipo de error que debe cerrar la conexión, luego para cada
error específico instanciamos esta clase. Serán estas instancias las que luego
se `raise`-arán dentro de los métodos específicos de cada comando hftp.

Luego de esto es cuestión de que el método de cada comando se encargue de
detectar cuando tiene que hacer un `raise` y a cual instancia de `HFTPError` (a
que error del hftp corresponde el `raise`).

### Conexión Multicliente

Una vez que todo estaba implementado la conexión multicliente fue relativamente
simple, haciendo que `Connection.handle` no sea lanzado en el main thread del
server, si no en uno nuevo.

Además hace falta alguna forma de registrar las conexiones que el server tiene
en cada momento, para esto se hizo un diccionario `connections` en `Server`,
donde las keys son los sockets (los sockets son hasheables), y los values la
respectiva instancia de `Connection` para ese socket.

Además corremos los threads con `daemon=True` ya que no queríamos tener que
manejar el caso en que se quiera cerrar el server, pero los threads de las
conexiones estén bloqueados esperando datos de los clientes. Con daemon=True, al
cerrarse el main thread con `Ctrl-C`, los otros mueren sin tener que
`join`-earlos. Otra opción para esto habría sido usar `select` en cada
`Connection` con un timeout.

## Dificultades

No todos teníamos el mismo nivel de conocimientos de python, y algunas de las
soluciones que usamos usan cosas específicas de python, eso complico un poco
algunas cosas.

Por otro lado la decisión de usar esas funcionalidad vino del problema de poder
manejar bien todos los posibles casos de una forma más o menos genérica. Eso en
sí fue otra dificultad.

## Preguntas

1. ¿Qué estrategias existen para poder implementar este mismo servidor pero con
capacidad de atender ​múltiples clientes simultáneamente​? Investigue y responda
brevemente qué cambios serían necesario en el diseño del código.

  Una opción es la de usar threads (o procesos), que es muy similar a lo que
  hicimos y explicamos más arriba.

  Otra forma habría sido tener un pool de los sockets de las conexiones y
  con `select` bloquear hasta que algún socket lea algo, y en ese caso se maneja
  la conexión que le correspondería a ese socket.

  Otra forma interesante es usando corutinas: con `asyncio` en python, y las
  keywords `async` y `await`. La idea es tener un main loop de eventos, donde
  uno va haciendo llamadas de la forma `await function()` y la función está
  definidia como `async def function()`, en este caso la llamada a la función
  queda encolada sin parar la ejecución del programa, y cuando el event loop
  decide que puede ejecutar `function` la ejecuta y vuelve a la línea que la
  había llamado originalmente. Un server implementaría se mas o menos así:

  ```python
  import asyncio

  async def handle():
      while True:
          mensaje = await nuevo_mensaje()
          procesar_mensaje(mensaje)

  async def main():
      while True:
          c = await nuevo_cliente()
          handle(c)  # Llamada asíncrona (no para la ejecución)

  asyncio.run(main())
  ```

  Algo a destacar es que a causa del **Global Lock Interpreter** (*GIL*) de
  python, el programa no puede ejecutarse en varios procesadores, así que la
  implementación de los threads en python son en realidad algo similar a las
  corutinas.

2. Pruebe ejecutar el servidor en una máquina del laboratorio, mientras utiliza
el cliente desde otra, hacia la ip de la máquina servidor. ¿Qué diferencia hay
si se corre el servidor desde la IP “localhost”, “127.0.0.1” o la ip “0.0.0.0”?

  El socket sólo va a recibir paquetes que vayan dirigidos a la ip con la que se
  creo. `127.0.0.1` (*y `localhost` que es un sinónimo, ver `/etc/hosts`*), es
  la ip que se le asigna a nuestro host, entonces esas dos direcciones sólo
  tienen sentido dentro de nuestra pc, por ende sólo nosotros podemos
  conectarnos al server. `0.0.0.0` por otro lado es una suerte de wildcard, que
  acepta paquetes con *destination address* a cualquier dirección, es por esto,
  que con esta dirección, pueden conectarse clientes desde fuera del host del
  server.
