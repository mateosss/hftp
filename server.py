#!/usr/bin/env python
# encoding: utf-8
# Revisión 2019 (a Python 3 y base64): Pablo Ventura
# Revisión 2014 Carlos Bederián
# Revisión 2011 Nicolás Wolovick
# Copyright 2008-2010 Natalia Bidart y Daniel Moisset
# $Id: server.py 656 2013-03-18 23:49:11Z bc $

import optparse
from threading import Thread
from connection import Connection
from socket import socket
from constants import *
from connection import *


class Server(object):
    """
    The server creates a listen socket, for receiving new connections.
    When a new client arrives, it creates a new socket for handling that client
    and a Connection object to handle it.
    """

    def __init__(self, addr=DEFAULT_ADDR, port=DEFAULT_PORT,
                 directory=DEFAULT_DIR):
        print(f"[Serving {directory} on {addr}:{port}]")
        self.socket = socket.socket()
        # Force reopening the socket even though TIME_WAIT flag is set
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((addr, port))
        self.socket.listen()
        self.default_dir = directory
        self.connections = {}  # {socket: Connection}
        self.next_name = 1

    def serve(self):
        """
        If a new client arrives, it is derived to a new socket in a Connection
        object which handles the client in another thread.
        """
        try:
            while True:
                sock, addr = self.socket.accept()
                conn = Connection(self, self.next_name, sock, self.default_dir)
                self.next_name += 1
                self.connections[sock] = conn
                Thread(target=conn.handle, daemon=True).start()
        except KeyboardInterrupt:
            print("\n[Closing server]")
            # Iterate over a copy of connections values to close/delete them
            for connection in list(self.connections.values()):
                connection.close()


def main():
    """Parsea los argumentos y lanza el server"""

    parser = optparse.OptionParser()
    parser.add_option(
        "-p", "--port",
        help="Número de puerto TCP donde escuchar", default=DEFAULT_PORT)
    parser.add_option(
        "-a", "--address",
        help="Dirección donde escuchar", default=DEFAULT_ADDR)
    parser.add_option(
        "-d", "--datadir",
        help="Directorio compartido", default=DEFAULT_DIR)

    options, args = parser.parse_args()
    if len(args) > 0:
        parser.print_help()
        sys.exit(1)
    try:
        port = int(options.port)
    except ValueError:
        sys.stderr.write(
            "Numero de puerto invalido: %s\n" % repr(options.port))
        parser.print_help()
        sys.exit(1)

    server = Server(options.address, port, options.datadir)
    server.serve()


if __name__ == '__main__':
    main()
