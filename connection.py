# encoding: utf-8
# Revisión 2019 (a Python 3 y base64): Pablo Ventura
# Copyright 2014 Carlos Bederián
# $Id: connection.py 455 2011-05-01 00:32:09Z carlos $

import socket
import os
from errno import ENOENT, EISDIR, ENAMETOOLONG
from constants import *
from base64 import b64encode


class HFTPError(Exception):
    def __init__(self, code, close=False):
        """
        Exception that binds an hftp error code to a response message
        :param close: if true, the connection will be marked for termination
        """
        self.close = close
        super().__init__(f"{code} {error_messages[code]}{EOL}")


# Instantiate hftp errors
BadEOL = HFTPError(BAD_EOL, close=True)
BadRequest = HFTPError(BAD_REQUEST, close=True)
InternalError = HFTPError(INTERNAL_ERROR, close=True)
InvalidCommand = HFTPError(INVALID_COMMAND)
InvalidArguments = HFTPError(INVALID_ARGUMENTS)
FileNotFound = HFTPError(FILE_NOT_FOUND)
BadOffset = HFTPError(BAD_OFFSET)


class Connection(object):
    """
    Point to point connection between server and client.
    It serves the client until the connection is closed.
    """

    def __init__(self, server, name, socket, directory):
        self.server = server
        self.name = name
        self.socket = socket
        # Create default directory if it doesn't exist
        if not os.path.exists(directory):
            os.makedirs(directory)
        self.directory = directory
        self.buffer = ""
        self.active = True

    def handle(self):
        "Handles connection loop with the client"
        print(f"[{self.name}][New Connection] directory={self.directory}")
        try:
            while self.active:
                recv = self.socket.recv(4096)
                print(f"\n[{self.name}][Recv] {recv[0:80]}")
                if len(recv) == 0:
                    raise ConnectionError
                self.buffer += recv.decode()
                print(f"[{self.name}][Buffer] {self.buffer.encode()[0:80]}")
                responses = self.process_buffer()
                for response in responses:
                    print(f"[{self.name}][Response] {response[0:80]}")
                    self.socket.send(response)
        except ConnectionError as e:  # Connection was closed by the client
            print(f"[{self.name}][WARNING] Unexpected connection end: {e}")
        except Exception as e:  # Other kind of exception happened
            print(f"[{self.name}][ERROR] {e}")
        self.close()

    def close(self):
        "Closes the socket, and deletes itself from the server"
        print(f"[{self.name}][Connection Closed]")
        self.socket.close()
        del self.server.connections[self.socket]

    def get_file_listing(self):
        "HFTP command: returns a list of files in the current directory"
        path = os.path.join(os.getcwd(), self.directory)
        files = os.listdir(path)
        return "".join([file + EOL for file in files]) + EOL

    def get_metadata(self, file_name):
        "HFTP command: returns the size of the file named file_name"
        try:
            file_name = os.path.join(os.getcwd(), self.directory, file_name)
            file_size = os.stat(file_name).st_size
            res = str(file_size) + EOL
        except OSError as e:
            if e.errno in [ENOENT, EISDIR, ENAMETOOLONG]:
                raise FileNotFound
            raise e
        return res

    def get_slice(self, file_name, offset, size):
        "HFTP command: returns file's [offset:size] slice encoded in base64"
        try:
            # Int arguments conversion
            try:
                offset = int(offset)
                size = int(size)
            except ValueError:
                raise InvalidArguments
            file_name = os.path.join(os.getcwd(), self.directory, file_name)
            file_size = os.stat(file_name).st_size
            # Out of file bounds?
            if offset < 0 or size < 0 or offset + size > file_size:
                raise BadOffset
            # Read the file and encode it
            file = open(file_name, "rb")
            file.seek(offset)
            res = file.read(size)
            res = b64encode(res).decode() + EOL
            file.close()
        except OSError as e:  # A syscall error
            if e.errno in [ENOENT, EISDIR, ENAMETOOLONG]:
                raise FileNotFound
            raise e  # Re-raise if we don't know what happened
        return res

    def quit(self):
        "HFTP command: marks this connection for termination"
        self.active = False
        return ""

    def process_buffer(self):
        """
        Consumes (parses) all the complete commands in the buffer (or up to a
        quit command), and returns a list with their respective responses
        """
        eolcount = self.buffer.count(EOL)
        fullcmds = self.buffer.split(EOL)[0:eolcount]
        responses = []
        for cmd in fullcmds:
            if self.active:  # Didn't we just quit?
                responses.append(self.parse(cmd))
        # Consume from buffer what was used
        if responses:
            newstart = self.buffer.rfind(EOL) + len(EOL)
            self.buffer = self.buffer[newstart:]
        return responses

    def handle_errors(parse):
        """
        Decorator that wraps a parsing function with exception management
        specific to HFTP, which is, instead of always crashing the server,
        return the proper error message to the client and close only the
        connection if needed
        """
        def error_handling_parse(self, *args, **kwargs):
            try:
                res = (
                    f"{CODE_OK} {error_messages[CODE_OK]}{EOL}"
                    f"{parse(self, *args, **kwargs)}"
                )
            except HFTPError as e:
                res = str(e)
                if e.close:
                    self.quit()
            except Exception as e:
                res = str(InternalError)
                print(f"[{self.name}][ERROR] {e}")
                self.quit()
            return res.encode()
        return error_handling_parse

    @handle_errors
    def parse(self, fullcmd):
        """
        Expects a clean single command (with arguments, without EOL), and
        performs input validation on the command, if all goes well, it executes
        the respective HFTP command method.
        """
        if '\n' in fullcmd:
            raise BadEOL

        tokens = fullcmd.split()

        if not tokens:
            raise BadRequest

        cmd = tokens[0]

        if cmd not in valid_commands:
            raise InvalidCommand

        if len(tokens) != valid_commands[cmd]:
            raise InvalidArguments  # Did not match expected argument count

        return getattr(self, cmd)(*tokens[1:])  # Call command function
